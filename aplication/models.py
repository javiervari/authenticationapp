from django.db import models

# Create your models here.

class University(models.Model):
	name = models.CharField(max_length=50)

	class Meta:
		verbose_name = "University"
		verbose_name_plural = "Universitys"

	def __str__(self):
		return self.name

class Student(models.Model):
	name = models.CharField(max_length=50)
	university = models.ForeignKey(University, on_delete=models.CASCADE)

	class Meta:
		verbose_name = "Student"
		verbose_name_plural = "Students"

	def __str__(self):
		return self.name

    