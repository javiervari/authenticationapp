from django.shortcuts import render
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from aplication.models import Student

# Create your views here.


class AppListView(LoginRequiredMixin, ListView):
	login_url = '/login/'
	template_name = 'home.html'
	queryset = Student.objects.all()
	


		