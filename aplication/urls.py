from django.urls import path
from aplication.views import AppListView


urlpatterns = [
    path('', AppListView.as_view(), name="home"),
]
