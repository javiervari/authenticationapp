from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput
from django.contrib.auth.models import User


class LoginForm(AuthenticationForm):
	username = forms.CharField(widget=TextInput(attrs={'class': 'input input--top','placeholder': 'Email'}))
	password = forms.CharField(widget=PasswordInput(attrs={'class': 'input','placeholder':'Password'}))


class UpdateUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields =  '__all__'