from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView,LogoutView
from django.views.generic import ListView, UpdateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from .form import LoginForm, UpdateUserForm
from django.shortcuts import get_object_or_404
from django.http import Http404


class LoginViewAuth(LoginView):
	template_name = "login_page.html"
	form_class = LoginForm
	redirect_authenticated_user = True

class LogoutViewAuth(LogoutView):
	pass




class ListAllUserAuth(LoginRequiredMixin, ListView):
	template_name = "list_all_user.html"
	queryset = User.objects.all()

class DetailUserAuth(LoginRequiredMixin, DetailView):
	template_name = "detail_user.html"
	context_object_name = "detail_user_object" #rename context
	def get_object(self):
		pk = self.kwargs.get("pk") #pk sending by url
		user = self.request.user.pk #host connected
		if pk==user or self.request.user.is_superuser: #only the same can be edited OR if Administrator, else 404
			return get_object_or_404(User, pk=pk)
		raise Http404

class EditUserAuth(LoginRequiredMixin, UpdateView):
	template_name = "edit_user.html"
	form_class = UpdateUserForm

	def get_object(self):
		pk = self.kwargs.get('pk')
		user = self.request.user.pk #host connected
		if pk==user or self.request.user.is_superuser: #only the same can be edited OR if Administrator, else 404
			return get_object_or_404(User, pk=pk)
		raise Http404

	

