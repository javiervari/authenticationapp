from django.urls import path
from .views import LoginViewAuth, LogoutViewAuth, ListAllUserAuth, DetailUserAuth, EditUserAuth

urlpatterns = [
    path('login/', LoginViewAuth.as_view(), name="login" ),
    path('logout/', LogoutViewAuth.as_view(), name="logout" ),

    path('list/', ListAllUserAuth.as_view(), name="list" ),
    path('detail/<int:pk>/', DetailUserAuth.as_view(), name="detail" ),
    path('edit/<int:pk>/', EditUserAuth.as_view(), name="edit" ),

]
